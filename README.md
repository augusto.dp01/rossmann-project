# Rossmann Store Sales
## Predicting sales for the Rossmann Company

![Rossmann Store](https://gitlab.com/augusto.dp01/rossmann-project/-/raw/master/img/rossmann.jpg)
Image: © Getty Images.

# 1.0 - Business Problem


**This case is inspired by and uses data from a past Kaggle Competition. Available [here](https://www.kaggle.com/c/rossmann-store-sales)**.

A big portion of Rossmann Drugstores are set to go through a renovating in their stores in six weeks, and the company's CFO needs to know how much of the receipt from this time to set aside for this work. For this, it is needed a prediction of sales for each store in the next six weeks. The CFO needs remote access to these predictions so they can make decisions on the go.

Data from each store sales day for over 2 years is given for this prediction.

# 2.0 - Business Assumptions

**01:** Promotions are a period in which a store generally lowers product prices and announces it in the market.

**02:** Stores have predetermined promotion periods, that are prolonged (by non determined periods) if results are good.

**03:** Stores with no 'competition_distance' are stores with no near competitor.

# 3.0 - Solution Strategy

The solution will be given through 10 steps. These are:

**01. Descriptive Data Analysis:** First we will use descriptive statistics techniques to get more familiarity with the data and eventually fix things in the dataframe, such as NA values or data types.

**02. Feature Engineering:** Then we will derive features from the ones that we have, so we can validate hypotheses in the Exploratory Data Analysis step, and have a better machine learning modeling. Since we need to know what features to derive, we will also develop our hypotheses.

**03. Variable Filtering:** Here we will drop columns and lines that are not useful for our analysis or are not available for prediction.

**04. Exploratory Data Analysis:** The main goal here is to know how each feature affects the response variable and how they relate to each other. We do this by validating the hypotheses developed in step 2.

**05. Data Preparation:** This step is geared towards the optimization of the machine learning process. We transform the data as to get it standardized and rescaled.

**06. Feature Selection:** We use an algorithm to determine which features are useful for prediction for dimentionality reductio purposes. After that we validate the algorithm's choices to see if it agrees with our exploratory data analysis.

**07. Machine Learning Modeling:** Here we train and cross validate models using the Occam's Razor criterion, from the less complexity models to the more sophisticated ones. Then we choose which model we will take to the deploy.

**08. Hyperparameter Fine Tunning:** In this step we run a search method to decide the best hyperparameters for our model.

**09. Interpreting the Error:** We take the measured error and translate them in our predictions, from our regular measures (MAE, RMSE) to more business suited units: percentages and currency (in this case, Euros), for better understanding of the company.

**10. Model Deploy:** Finally we write the code for every step of our pipeline and deploy it on the internet so people can access our results.

# 4.0 - Data Insights

## 4.1 - Stores should sell more in the end of the year: TRUE
We can see that by the end of the year there is a big spike in sales. In fact, there is a reasonably high correlation between the number of the month and the amount of sales!

![end of year](https://gitlab.com/augusto.dp01/rossmann-project/-/raw/master/img/year_end.png)

## 4.2 - Stores sell more after the 10th of each month: FALSE
This hypothesis come from the idea that clients receive salaries around that time of the month. Even though we can see some raise in sales after day 10, the raise is not enough to surpass neither the end or the start of the month. In fact, there is a negative correlation between the day of the month and the sales.

![10th of each month](https://gitlab.com/augusto.dp01/rossmann-project/-/raw/master/img/10th_each_month.png)

## 4.3 - Stores with near competition for longer should sell more: FALSE
We thought that the impact of the opening of a competitor would drag down the sales, but this would fade. Actually the opposite happens: we see a raise in sales just before the opening of a competitor, with a peak just when the competitor opens. The sales then enter in a long downtrend after that, with no signs of sales recovery, contrary to expected.

![competition time](https://gitlab.com/augusto.dp01/rossmann-project/-/raw/master/img/competition_time.png)

The raise in sales before a competitor open can't be *because* of the competitor opening, but rather the raise in sales drives the competitor to open the store.

# 5.0 - Machine Learning Models

The models were implemented in a raising level of complexity, using the Occam's Razor in our analysis. The implemented models were:

**Linear Regression**

**Lasso Regression**

**Random Forest Regressor**

**XGBoost Regressor**

# 6.0 - Machine Leaning Performance

The lower complexity models did not perform well, so we choose XGBoost as our implementing algorithm for it's lightweight and fast training features.

The table below shows the results of the cross validation tests on each model before fine tuning. The cross validation used 6 slices from the end of the full set.

| Model Name | MAE CV | MAPE CV | RMSE CV |
| ------------ | ------------ | ------------ | ------------ |
|Linear Regression|2045.15 +/- 278.74|0.3 +/- 0.02|2884.67 +/- 447.87|
|Linear Regression Regularized|2071.84 +/- 328.11|0.29 +/- 0.01|2978.8 +/- 494.54|
|Random Forest Regressor|797.09 +/- 222.67|0.11 +/- 0.02|1186.37 +/- 334.12|
|XGBoost Regressor|1007.18 +/- 177.91|0.14 +/- 0.02|1449.04 +/- 251.63|


Even though the XGBoost hasn't the lower error, after the fine tuning it became better than the Random Forest. The fine tuning method was a cross validated random search. The table shows the XGBoost's performance after the fine tuning.

| Model Name | MAE CV | MAPE CV | RMSE CV |
| ------------ | ------------ | ------------ | ------------ |
XGBoost Regressor|766.65 +/- 156.99|0.11 +/- 0.02|1109.25 +/- 240.31

# 7.0 - Business Results

As a group, the predictions are very accurate. Here is a sample of the results:

|store|predictions|worst_scenario|best_scenario|MAE|MAPE|
| ----- | ----- | ----- | ----- | ----- | ----- |
| 308 | 316145.500000 | 311527.381255 | 320763.618745 | 769.686457 | 0.089053 |
|546|505002.656250|495987.021696|514018.290804|1502.605759|0.102577|
|621|214429.703125|211359.866580|217499.539670|511.639424|0.087320|
|273|272468.187500|269361.694389|275574.680611|517.748852|0.066669|
|801|195848.468750|192751.015665|198945.921835|516.242181|0.089478|

The median error for the predictions is under **10%**, and with sales over the time we are predicting, the difference between the best and worst scenarios is small, hence the results are well suited for the expected end!

Here is a box plot of the error distribution:

![mape distribution](https://gitlab.com/augusto.dp01/rossmann-project/-/raw/master/img/mape_box.png)

# 8.0 - The Telegram Bot

The Telegram Bot is accessible by anyone through [here](https://t.me/rossmann_sales_predict_bot). To use it you just send a message in the form "/id", where "id" is the id of the store you want to know about the sales. Stores ids go from 1 to 1115.

![bot sample](https://gitlab.com/augusto.dp01/rossmann-project/-/raw/master/img/bot.png)

# 9.0 - Next Steps to Improve

**1. Model for outlier stores:** The error was very behaved and controlable along most of the stores, but there were some outliers with MAPE of over 50%. This hints us that these stores behave very differently from the others. An in-depth analysis of them are needed, and possibly a model specific for them.

**2. Better GUI - Telegram:** For now, our bot only answers the amount that will be sold. More features such as other interactions to see charts or specific days of sales can be implemented in next cycles.

**3. Better Visualization - Dashboard:** The only way to access the result from the prediction is through Telegram. A dashboard using libraries such as Streamlit can be implemented and should be a lot more useful.
